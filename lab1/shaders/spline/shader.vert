#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 vColor;

out vec4 vertexColor;
uniform mat4 pvm;

void main()
{
   gl_Position = pvm * vec4(aPos, 1.0);
   vertexColor = vec4(vColor, 1.0);
}