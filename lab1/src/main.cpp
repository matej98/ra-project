#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stb_image.h>
#include <camera.h>
#include <shader.h>
#include <model.h>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

void mouse_callback(GLFWwindow *window, double xpos, double ypos);

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

void processInput(GLFWwindow *window);

glm::vec3 point(glm::vec3 *controlPoints, int s, float t);

glm::vec3 tangent(glm::vec3 *controlPoints, int s, float t);

void generateSpline(glm::vec3 *controlPoints, int segmentsCount, int partsPerSegment, glm::vec3 color, float *vertices);

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

const char *modelVertexShaderPath = "../shaders/model/shader.vert";
const char *modelFragmentShaderPath = "../shaders/model/shader.frag";
const char *modelPath = "../objects/porsche.obj";
const char *splineVertexShaderPath = "../shaders/spline/shader.vert";
const char *splineFragmentShaderPath = "../shaders/spline/shader.frag";

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;    // time between current frame and last frame
float lastFrame = 0.0f;

bool isAnimationStarted = false;
float timeSinceStarted = 0.0f;
float animationDuration = 10.0f;

float splineScaleFactor = 0.3f;

int main() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    GLFWwindow *window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    stbi_set_flip_vertically_on_load(true);

    glEnable(GL_DEPTH_TEST);

    Shader modelShader(modelVertexShaderPath, modelFragmentShaderPath);
    Shader splineShader(splineVertexShaderPath, splineFragmentShaderPath);

    // load models
    // -----------
    Model ourModel(modelPath);
//
    glm::vec3 controlPoints[] = {
            glm::vec3(0, 0, 0),
            glm::vec3(0, 10, 5),
            glm::vec3(10, 10, 10),
            glm::vec3(10, 0, 15),
            glm::vec3(0, 0, 20),
            glm::vec3(0, 10, 25),
            glm::vec3(10, 10, 30),
            glm::vec3(10, 0, 35),
            glm::vec3(0, 0, 40),
            glm::vec3(0, 10, 45),
            glm::vec3(10, 10, 50),
            glm::vec3(10, 0, 55)
    };
    auto splineColor = glm::vec3(1.0f, 1.0f, 1.0f);

    int segmentsCount = sizeof(controlPoints) / sizeof(glm::vec3) - 3;
    int partsPerSegment = 100;

    int verticesCount = partsPerSegment * segmentsCount;
    float splineVertices[verticesCount * 3 * 2];
    generateSpline(controlPoints, segmentsCount, partsPerSegment, splineColor, splineVertices);

    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(splineVertices), splineVertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) 0);
    glEnableVertexAttribArray(0);
    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);

    // draw in wireframe
//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    float animationSegmentDuration = animationDuration / segmentsCount;

    int currentSegment = 0;
    float t = 0.0f;

    auto splineStartPosition = point(controlPoints, currentSegment, t);

    while (!glfwWindowShouldClose(window)) {
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // don't forget to enable modelShader before setting uniforms
        splineShader.use();

        // view/projection transformations
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float) SCR_WIDTH / (float) SCR_HEIGHT, 0.1f,
                                                100.0f);
        glm::mat4 view = camera.GetViewMatrix();

        glm::mat4 splineModel = glm::mat4(1.0f);
        splineModel = glm::translate(splineModel, -splineStartPosition * splineScaleFactor);
        splineModel = glm::scale(splineModel, glm::vec3(splineScaleFactor, splineScaleFactor, splineScaleFactor));

        splineShader.setMat4("pvm", projection * view * splineModel);

        glBindVertexArray(VAO);
        glDrawArrays(GL_LINE_STRIP, 0, verticesCount);

        modelShader.use();

        glm::mat4 objectModel = glm::mat4(1.0f);

        if (isAnimationStarted) {
            timeSinceStarted += deltaTime;
            if (timeSinceStarted > animationDuration) {
                isAnimationStarted = false;
                timeSinceStarted = 0.0f;
            }
            currentSegment = timeSinceStarted / animationSegmentDuration;
            t = (timeSinceStarted - currentSegment * animationSegmentDuration) / animationSegmentDuration;
        }

        auto position = point(controlPoints, currentSegment, t);
        auto orientation = tangent(controlPoints, currentSegment, t);

        auto axis = glm::vec3(0.0f, 0.0f, 1.0f);
        auto vectorProduct = glm::cross(axis, orientation);
        auto scalarProduct = glm::dot(orientation, axis);
        auto angle = glm::acos(scalarProduct / glm::length(orientation) / glm::length(axis));

        objectModel = glm::translate(objectModel, -splineStartPosition * splineScaleFactor);
        objectModel = glm::scale(objectModel, glm::vec3(splineScaleFactor, splineScaleFactor, splineScaleFactor));
        objectModel = glm::translate(objectModel, position);
        objectModel = glm::rotate(objectModel, angle, vectorProduct);
        objectModel = glm::scale(objectModel, glm::vec3(1.0f, 1.0f, 1.0f) / splineScaleFactor);
        modelShader.setMat4("pvm", projection * view * objectModel);
        ourModel.Draw(modelShader);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
        isAnimationStarted = !isAnimationStarted;
        glfwGetTime();
    }
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    camera.ProcessMouseScroll(yoffset);
}

glm::vec3 point(glm::vec3 *controlPoints, int s, float t) {
    glm::mat4 B(
            -1, 3, -3, 1,
            3, -6, 3, 0,
            -3, 0, 3, 0,
            1, 4, 1, 0
    );
    B /= 6.0f;
    glm::mat4x3 R(
            controlPoints[s],
            controlPoints[s + 1],
            controlPoints[s + 2],
            controlPoints[s + 3]
    );
    glm::vec4 T(t * t * t, t * t, t, 1);
    glm::vec3 point = R * B * T;
    return point;
}

glm::vec3 tangent(glm::vec3 *controlPoints, int s, float t) {
    glm::mat3x4 B(
            -1, 3, -3, 1,
            2, -4, 2, 0,
            -1, 0, 1, 0
    );
    B /= 2.0f;
    glm::mat4x3 R(
            controlPoints[s],
            controlPoints[s + 1],
            controlPoints[s + 2],
            controlPoints[s + 3]
    );
    glm::vec3 T(t * t, t, 1);
    glm::vec3 tangent = R * B * T;
    return tangent;
}

void generateSpline(glm::vec3 *controlPoints, int segmentsCount, int partsPerSegment, glm::vec3 color, float *vertices) {
    for (int i = 0; i < segmentsCount; i++) {
        for (int j = 0; j < partsPerSegment; ++j) {
            float t = j / (partsPerSegment - 1.0f);
            auto p = point(controlPoints, i, t);
            int vertexPosition = (i * partsPerSegment + j) * 3 * 2;
            vertices[vertexPosition] = p.x;
            vertices[vertexPosition + 1] = p.y;
            vertices[vertexPosition + 2] = p.z;
            vertices[vertexPosition + 4] = color.x;
            vertices[vertexPosition + 5] = color.y;
            vertices[vertexPosition + 6] = color.z;
        }
    }
}
