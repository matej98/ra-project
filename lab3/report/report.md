# 2D elastična lopta ostvarena sustavom masa i opruga

## Opis sustava

U samostalnoj laboratorijskoj vježbi izrađen je sustav masa i opruga. Ukupno `n` masa raspoređeno je na kružnici te su
povezane s ukupno `n(n-1)/2` opruga, svaka masa sa svakom drugom. U svakom ciklusu računa se duljina opruge kao razlika
između dviju masa koje su povezane tom oprugom. Iz duljine opruge računa se sila na obje mase. Ukupna sila na masu
određuje akceleraciju, akceleracija promjenu brzine te brzina novi položaj mase. Pritom se koristi poluimplticitni
Eulerov postupak integracije. Ukoliko se za neku masu detektira kolizija s rubom prozora, komponenta brzine okomita na
rub prozora se postavlja na nulu. U smjeru suprotnom od komponente brzine paralene s rubom prozora dodaje se sila koja
simulira trenje. Pomoću ta dva postupka samo svojstvo odbijanja postiže se implicitno pomoću sila kojima oruge dijeluju
na mase te se dobiva kotrljanje i rotacija zbog utjecaja trenja.

Prikaz je ostvaren pomoću OpenGL-a kao lopta koja se odbija od zidova (rubova prozora). Korištena je tekstura lopte na
na način da je svaka masa direktno vezana uz neku poziciju u teksturi. Time se ostvaruje dojam elastičnosti teksture
prilikom deformacije pojedinih opruga. Teško je postići da elastičnost izgleda potpuno prirodno jer bi trebalo povezati
mase sa teksturom na dinamičan način, budući da sustav masa i opruga ne opisuje dovoljno kvalitetno ponašanje gumene
lopte.

Kako bi se omogućilo kreanje lopte u prozoru, pomoću strelica na tipkovnici moguće je dodati dodatnu silu u svakom od
četiri smjera. Dodatne sile se zbrajaju na ukupnu silu na svaku masu. Odabran je pristup sa strelicama zbog
jednostavnije implementacije u odnosu na pomicanje lopte pomoću miša.

### Primjeri

Opruge u slijedećim primjerima imaju postavljenu značajno manju vrijednost konstante kako bi se istaknuo efekt
elastičnosti.
![](./images/ball_center.png)
*Lopta u sredini prozora*
![](./images/ball_down.png)
*Lopta prilikom kolizije s donjim rubom*
![](./images/ball_left_down.png)
*Lopta prilikom kolizije s donjim lijevim kutom*

## Upute za pokretanje

Projekt je izrađen koristeći [CLion](https://www.jetbrains.com/clion/), ali naravno da je moguće projekt pokrenuti s
bilo kojim drugim IDE-om. Korišten je [cmake](https://cmake.org/) za build proces. Dodatno je potrebno instalirati
[glfw](https://www.glfw.org/download.html), a sve ostale ovisnosti su uključene kao header datoteke unutar projekta.