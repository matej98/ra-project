#include <glm/geometric.hpp>
#include <vector>
#include <unordered_set>

class Particle
{
private:
	float left = -1;
	float right = 1;
	float top = 1;
	float bottom = -1;

	float collisionVelocityFactor = 0;
	float wallFrictionFactor = 0.5;
	float airFrictionFactor = 0.1;
public:
	glm::vec2 force;
	glm::vec2 velocity;
	glm::vec2 position;
	float mass = 0.05;

	void update(float deltaTime)
	{
		force -= velocity * airFrictionFactor * deltaTime;
		velocity += force * deltaTime / mass;
		position += velocity * deltaTime;

		if (position.x <= left)
		{
			position.x = left;
			if (velocity.x < 0)
			{
				velocity.x *= collisionVelocityFactor;
				velocity.y *= wallFrictionFactor;
			}
		}
		if (position.x >= right)
		{
			position.x = right;
			if (velocity.x > 0)
			{
				velocity.x *= collisionVelocityFactor;
				velocity.y *= wallFrictionFactor;
			}
		}
		if (position.y <= bottom)
		{
			position.y = bottom;
			if (velocity.y < 0)
			{
				velocity.y *= collisionVelocityFactor;
				velocity.x *= wallFrictionFactor;
			}
		}
		if (position.y >= top)
		{
			position.y = top;
			if (velocity.y > 0)
			{
				velocity.y *= collisionVelocityFactor;
				velocity.x *= wallFrictionFactor;
			}
		}

		force = {0, 0};
	}
};

class Spring
{
public:
	Particle &p1, &p2;
	float originalLength;
	float length = 0;
	glm::vec2 direction12;

	explicit Spring(Particle &p1, Particle &p2) :
			p1(p1),
			p2(p2)
	{
		originalLength = glm::length(p1.position - p2.position);
	}

	void update()
	{
		auto diff = p2.position - p1.position;
		length = glm::length(diff);
		direction12 = diff / length;
	}
};


enum Direction
{
	DIRECTION_UP, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT
};

class System
{
public:
	static const int numberOfParticles = 16;
private:
	std::vector<Spring> springs;
	Particle particles[numberOfParticles];
	float radius = 0.1f;
	float springStiffness = 15.0;
	float springDamping = 0.2;
	std::unordered_set<Direction> additionalForceDirections;
public:
	System()
	{
		double deltaAngle = 2 * M_PI / numberOfParticles;
		for (int i = 0; i < numberOfParticles; ++i)
		{
			double angle = i * deltaAngle;
			float x = (float) std::cos(angle) * radius;
			float y = (float) std::sin(angle) * radius;
			particles[i].position = {x, y};
		}

		for (int i = 0; i < numberOfParticles; ++i)
		{
			for (int j = i + 1; j < numberOfParticles; ++j)
			{
				springs.emplace_back(particles[i], particles[j]);
			}
		}
	}

	void updateParticles(float deltaTime)
	{
		for (auto &spring: springs)
		{
			spring.update();
			auto forceValue = springStiffness * (spring.length - spring.originalLength);
			forceValue -= springDamping * glm::dot(spring.p1.velocity - spring.p2.velocity, spring.direction12);
			auto force = forceValue * spring.direction12;
			spring.p1.force += force;
			spring.p2.force -= force;
		}

		glm::vec2 additionalForce;
		for (auto &direction: additionalForceDirections)
		{
			switch (direction)
			{
				case DIRECTION_UP:
					additionalForce += glm::vec2(0, 1);
					break;
				case DIRECTION_DOWN:
					additionalForce += glm::vec2(0, -1);
					break;
				case DIRECTION_LEFT:
					additionalForce += glm::vec2(-1, 0);
					break;
				case DIRECTION_RIGHT:
					additionalForce += glm::vec2(1, 0);
					break;
			}
		}
		additionalForce *= 0.1f;

		for (auto &particle: particles)
		{
			particle.force += additionalForce;
			particle.update(deltaTime);
		}

		additionalForceDirections.clear();
	}

	glm::vec2 getPosition(int i)
	{
		return particles[i].position;
	}

	void enableForceInDirection(Direction direction)
	{
		additionalForceDirections.insert(direction);
	}
};