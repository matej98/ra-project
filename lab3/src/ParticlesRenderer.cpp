#include <shader.h>
#include "System.cpp"
#include "stb_image.h"

class ParticlesRenderer
{
private:
	Shader shader = Shader("shaders/shader.vert", "shaders/shader.frag");

	static const int valuesPerVertex = 4;
	static const int numberOfVertices = valuesPerVertex * (System::numberOfParticles + 2);
	float vertices[numberOfVertices];

	unsigned int VBO, VAO;
	unsigned int texture;
public:
	ParticlesRenderer()
	{
		createVertices();
		loadTexture();
	}

	void render(System particles)
	{
		shader.use();

		glm::vec2 center;
		for (int i = 0; i < System::numberOfParticles; ++i)
		{
			auto position = particles.getPosition(i);
			vertices[(i + 1) * 4 + 0] = position.x;
			vertices[(i + 1) * 4 + 1] = position.y;
			center += position;
		}
		center /= System::numberOfParticles;
		vertices[0] = center.x;
		vertices[1] = center.y;
		auto position = particles.getPosition(0);
		vertices[(System::numberOfParticles + 1) * 4 + 0] = position.x;
		vertices[(System::numberOfParticles + 1) * 4 + 1] = position.y;

		drawVertices();
	}

private:
	void createVertices()
	{
		double deltaAngle = 2 * M_PI / System::numberOfParticles;
		for (int i = 0; i < System::numberOfParticles; ++i)
		{
			double angle = i * deltaAngle;
			float x = (float) std::cos(angle) * 0.5f + 0.5f;
			float y = (float) std::sin(angle) * 0.5f + 0.5f;
			vertices[(i + 1) * 4 + 2] = x;
			vertices[(i + 1) * 4 + 3] = y;
		}
		vertices[2] = 0.5;
		vertices[3] = 0.5;
		vertices[(System::numberOfParticles + 1) * 4 + 2] = 1;
		vertices[(System::numberOfParticles + 1) * 4 + 3] = 0.5;

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, valuesPerVertex * sizeof(float), (void *) 0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, valuesPerVertex * sizeof(float), (void *) (2 * sizeof(float)));
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	void loadTexture()
	{
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		int width, height, nrChannels;

		unsigned char *data = stbi_load("textures/football.png", &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		} else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);
	}

	void drawVertices()
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindTexture(GL_TEXTURE_2D, texture);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLE_FAN, 0, System::numberOfParticles + 2);
	}
};