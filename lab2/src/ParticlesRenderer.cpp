#include <shader.h>
#include <glm/gtc/matrix_transform.hpp>
#include "Particles.cpp"
#include <stb_image.h>


class ParticlesRenderer {
private:
    Particles particles;
    Shader shader = Shader("../shaders/particles/shader.vert", "../shaders/particles/shader.frag");

    const unsigned int verticesPerParticle = 6;
    const unsigned int valuesPerVertex = 5;
    unsigned long sizeOfVertices = (sizeof(float)) * valuesPerVertex * verticesPerParticle * particles.getCount();
    float *vertices = static_cast<float *>(malloc(sizeOfVertices));

    unsigned int VBO, VAO;
    unsigned int texture = loadTexture("../textures/snow.png");
public:
    ParticlesRenderer(Particles particles) : particles(particles) {
        createVertices();
    }

    void render(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition) {
        float size =0.01f;
        float square[] = {
                size, size, 0.0f, 1.0f, 1.0f, // top right
                size, -size, 0.0f, 1.0f, 0.0f, // bottom right
                -size, size, 0.0f, 0.0f, 1.0f,  // top left
                size, -size, 0.0f, 1.0f, 0.0f, // bottom right
                -size, -size, 0.0f, 0.0f, 0.0f, // bottom left
                -size, size, 0.0f, 0.0f, 1.0f,  // top left
        };

        shader.use();
        unsigned int activeParticlesCount = 0;
        unsigned int j = 0;

        for (int i = 0; i < particles.getCount(); ++i) {
            auto particle = particles.getParticle(i);

            if (!particle.isActive) continue;

            glm::mat4 model = glm::mat4(1.0f);

            auto orientation = cameraPosition - particle.position;

            auto axis = glm::vec3(0.0f, 0.0f, 1.0f);
            auto vectorProduct = glm::cross(axis, orientation);
            auto scalarProduct = glm::dot(orientation, axis);
            auto angle = glm::acos(scalarProduct / glm::length(orientation) / glm::length(axis));

//            model = glm::rotate(model, angle, vectorProduct);
            model = glm::scale(model, glm::vec3(particle.size, particle.size, particle.size));
            model = glm::translate(model, particle.position);

            auto position = glm::vec4(particle.position, 1.f);
            position = model * position;
            unsigned int l = 0;
            for (int k = 0; k < verticesPerParticle; ++k) {
                vertices[j++] = position.x + square[l++];
                vertices[j++] = position.y + square[l++];
                vertices[j++] = position.z + square[l++];
                vertices[j++] = square[l++];
                vertices[j++] = square[l++];
            }

            activeParticlesCount++;
        }

        shader.setMat4("pvm", projection * view);

        drawVertices(activeParticlesCount * 6);
    }

private:
    void createVertices() {
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeOfVertices, NULL, GL_DYNAMIC_DRAW);

        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) 0);
        glEnableVertexAttribArray(0);
        // texture coord attribute
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) (3 * sizeof(float)));
        glEnableVertexAttribArray(1);
    }

    static unsigned int loadTexture(char const *path) {
        unsigned int textureID;
        glGenTextures(1, &textureID);

        int width, height, nrComponents;
        unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
        if (data) {
            GLenum format;
            if (nrComponents == 1)
                format = GL_RED;
            else if (nrComponents == 3)
                format = GL_RGB;
            else if (nrComponents == 4)
                format = GL_RGBA;

            glBindTexture(GL_TEXTURE_2D, textureID);
            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            stbi_image_free(data);
        } else {
            std::cout << "Texture failed to load at path: " << path << std::endl;
            stbi_image_free(data);
        }

        return textureID;
    }

    void drawVertices(int count) {
        glBindTexture(GL_TEXTURE_2D, texture);
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeOfVertices, vertices);
        glDrawArrays(GL_TRIANGLES, 0, count);
    }
};