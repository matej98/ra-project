#include <cstdlib>
#include <glm/vec3.hpp>
#include <glm/geometric.hpp>

class Particle {
public:
    glm::vec3 position;
    glm::vec3 velocity;
    float size;
    bool isActive;
    float lifeTime;
};

class Source {
public:
    glm::vec3 gravity;
    int cornersCount;
    glm::vec3 *corners;
};

class Particles {

private:
    int count = 0;
    Particle *particles = nullptr;
    Source source;
    int particlesPerSecond;
    float particlesCreationInterval;
    float particleLifeTime;
    float timeSinceLastParticle = 0;
public:
    Particles(int particlesPerSecond, float lifeTime, Source particlesSource) :
            particlesPerSecond(particlesPerSecond),
            particleLifeTime(lifeTime),
            source(particlesSource) {
        particlesCreationInterval = 1.0f / particlesPerSecond;
        count = (int) (lifeTime * particlesPerSecond);
        particles = (Particle *) malloc(sizeof(Particle) * count);
        reset();
    }

    void reset() {
        for (int i = 0; i < count; ++i) {
            resetParticle(i);
        }
    }

    void updateParticles(float deltaTime) {
        int particlesNeededInDeltaTime = particlesPerSecond * deltaTime;
        int particlesActivatedInDeltaTime = 0;
        timeSinceLastParticle += deltaTime;
        for (int i = 0; i < count; ++i) {
            if (particles[i].isActive) {
                if (particles[i].lifeTime > particleLifeTime) {
                    deactivateParticle(i);
                } else {
                    updateParticle(i, deltaTime);
                }
            } else {
                if (particlesNeededInDeltaTime > 0) {
                    if (particlesActivatedInDeltaTime < particlesNeededInDeltaTime) {
                        resetParticle(i);
                        activateParticle(i);
                        particlesActivatedInDeltaTime++;
                    }
                } else if (timeSinceLastParticle > particlesCreationInterval) {
                    resetParticle(i);
                    activateParticle(i);
                    timeSinceLastParticle = 0;
                }
            }
        }
    }

    Particle getParticle(int i) {
        return particles[i];
    }

    int getCount() {
        return count;
    }

private:
    void resetParticle(int i) {
        auto position = randomPositionInSource();
        particles[i].position.x = position.x;
        particles[i].position.y = position.y;
        particles[i].position.z = position.z;
        particles[i].velocity.x = 0;
        particles[i].velocity.y = 0;
        particles[i].velocity.z = 0;
        particles[i].size = randomSize();
        particles[i].lifeTime = 0;
        particles[i].isActive = false;
    }

    void activateParticle(int i) {
        particles[i].isActive = true;
    }

    void deactivateParticle(int i) {
        particles[i].isActive = false;
    }

    glm::vec3 randomPositionInSource() {
        glm::vec3 position;
        float weightsSum = 0;
        for (int i = 0; i < source.cornersCount; ++i) {
            float weight;
            if (i == source.cornersCount - 1) {
                weight = 1 - weightsSum;
            } else {
                weight = random(0, 1 - weightsSum);
            }
            position += source.corners[i] * weight;
        }
        return position;
    }

    float randomSize() {
        return random(0.5, 1.5);
    }

    void updateParticle(int i, float deltaTime) {
        auto size = particles[i].size;
        auto velocity = particles[i].velocity + source.gravity * deltaTime + glm::vec3(randomOffset(size), randomOffset(size), 0);
        auto speed = glm::length(velocity);
        auto maxSpeed = maxSpeedForSize(particles[i].size);
        if (speed > maxSpeed) {
            velocity = glm::normalize(velocity) * maxSpeed;
        }
        auto position = particles[i].position + velocity * deltaTime;
        particles[i].position = position;
        particles[i].velocity = velocity;
        particles[i].lifeTime += deltaTime;
    }

    float maxSpeedForSize(float size) {
        return 1.5f * size;
    }

    float randomOffset(float size) {
        float bound = 0.5f / size;
        float r = random(-bound, bound);
        return r * r * r;
    }

    float random(float from, float to) {
        return (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * (to - from) + from;
    }
};